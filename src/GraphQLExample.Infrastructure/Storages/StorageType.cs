namespace GraphQLExample.Infrastructure
{
    public enum StorageType
    {
        InMemory = 0,
        Mongo = 1,
        EntityFramework = 2
    }
}